<?php

require 'JpGraph.php';

$jpGraphVersion = \JpGraph\JpGraph::$version;
$fontInstallPath = __DIR__ . "/jpgraph-$jpGraphVersion/src/fonts/";

$selectedFont = [
  "Montserrat-Light.ttf", // NORMAL
  "Montserrat-SemiBold.ttf", // BOLD
  "Montserrat-SemiBoldItalic.ttf", // BOLD ITALIC
  "Montserrat-LightItalic.ttf" // ITALIC
];

$zip = new ZipArchive();
$res = $zip->open('Montserrat.zip');
if ($res === TRUE) {
  $zip->extractTo($fontInstallPath, $selectedFont);
  $zip->close();

  rename($fontInstallPath . $selectedFont[0], $fontInstallPath . "DejaVuSans.ttf");
  rename($fontInstallPath . $selectedFont[1], $fontInstallPath . "DejaVuSans-Bold.ttf");
  rename($fontInstallPath . $selectedFont[2], $fontInstallPath . "DejaVuSans-BoldOblique.ttf");
  rename($fontInstallPath . $selectedFont[3], $fontInstallPath . "DejaVuSans-Oblique.ttf");

  echo 'Font Installed';
} else {
  echo 'Error while installing custom font !';
}